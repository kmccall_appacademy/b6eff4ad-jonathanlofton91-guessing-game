# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
   array = (1..100).to_a
  random_num = array.sample

  correct_answer = false
  guess_count = 0
  until correct_answer
    print "guess a number"
    user_guess = gets.to_i
    p user_guess

    if user_guess == random_num
      correct_answer = true
    elsif user_guess > random_num
      print "too high"
    elsif user_guess < random_num
      print "too low"
    end
    guess_count += 1
  end

  print "Conrgrats! #{random_num} was the correct answer and it took #{guess_count}
  guesses"
end
